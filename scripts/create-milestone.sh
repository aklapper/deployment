# 2022-07-21: Removing the tag plugin and its dependency in asciitable, since
# code is in an unusable state, and breaks on the deployment server.
#
# TODO: Revisit this once scap deploys are sorted out.

scap tag milestone --project phabricator --template ./templates/phab.tmpl --hashtag phab-$1 $1
